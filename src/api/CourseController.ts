import { Request, Response } from 'express';
import { IService } from '../definition/IService';
import { IController } from '../definition/IController';
import 'reflect-metadata';
import { TYPES } from '../ioc/types';
import { inject, injectable } from 'inversify';

@injectable()
export class CourseController implements IController{

    constructor(@inject(TYPES.IService) private service: IService){
    }

    getAll(request: Request, response: Response): void {
        response.json(this.service.getAll());
    }
}