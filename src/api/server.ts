import 'reflect-metadata';
import http, { Server as HttpServer } from 'http';
import express, {Application, Request, Response} from 'express';
import { Routes } from './routes';
import { Middleware as midware } from '../middleware/middleware';
import morgan from 'morgan';
import cors from 'cors';
import container from '../ioc/inversify.config';

export class Server{
    private server!: HttpServer;
    private readonly port= process.env.PORT || 3001;

    constructor(){ }

    initServer(): void {
        this.server = http.createServer( this.createExpressServer());               
        this.server.listen(this.port, ()=> console.log('listening on port', this.port));
    }

    private createExpressServer(){
        const routes = container.get<Routes>('Routes');
        
        const app: Application = express();
        app.use(cors());
        app.use(express.json());
        app.use(morgan('tiny'));
        app.use('/users', routes.users);
        //app.use('/courses', midware.authorization, routes.courses);
        app.use('/courses', routes.courses);

        return app;
    }
}
