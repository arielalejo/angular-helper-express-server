import 'reflect-metadata';
import {Router, Request, Response} from 'express';
import container from '../ioc/inversify.config';
import { TYPES } from '../ioc/types';
import { IController } from '../definition/IController';
import { inject, injectable } from 'inversify';


// token with admin role
const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyLCJpc0FkbWluIjp0cnVlfQ.RPqW239apZm6Nt3pAnX-HXS0HvFDSd0hQ6jL3JPPkc4';

// token without admin role
// const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c';

@injectable()
export class Routes{
    private readonly courseController;
    private coursesRouter: Router;
    private usersRouter: Router;

    constructor(@inject(TYPES.IController) courseCntrll: IController){
        this.courseController = courseCntrll;
        this.initRoutes();
    }

    get courses (): Router {return this.coursesRouter;}

    get users (): Router {return this.usersRouter;}

    private initRoutes() :void{
        this.coursesRouter = Router();
        this.usersRouter = Router();

        this.coursesRouter.get('/', (req: Request, res: Response) => this.courseController.getAll(req,res));

        this.usersRouter.post('/', (req: Request, res: Response) => {
            const body = req.body;
            if (body.email === 'ariel@mail.com' && body.password === 'asdf')
                res.send({token: token}); 
            else 
                res.status(400).send('error');
        } );
    }

}
