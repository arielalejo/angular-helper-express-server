import { Container } from 'inversify';
import { IService } from '../definition/IService';
import { TYPES } from './types';
import { CourseService } from '../api/CourseService';
import { IController } from '../definition/IController';
import { CourseController } from '../api/CourseController';
import { Routes } from '../api/routes';

const container = new Container();

container.bind<Routes>('Routes').to(Routes);
container.bind<IController>(TYPES.IController).to(CourseController);
container.bind<IService>(TYPES.IService).to(CourseService);

export default container;