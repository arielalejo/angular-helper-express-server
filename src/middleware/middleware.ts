import { NextFunction, Request, Response } from 'express';
import  jwt  from 'jsonwebtoken';

const jwtPrivateKey = 'insecure-key';

export class Middleware{
    static authorization(req: Request, res: Response, next: NextFunction){
        const token = req.header('x-auth-token');
        if (!token){
            res.status(401).send('access denied, no token');
            return;
        }

        try {
            const decoded = token && jwt.verify(token, jwtPrivateKey);
            req.user = decoded;
            next();
        } catch (error) {
            res.status(400).send('invalid token');
        }
    }

    static admin(req: Request, res: Response, next: NextFunction){
        if (!req.user.isAdmin){
            res.status(403).send('forbiden');
            return;
        }

        next();
    }



}